#pragma once

#define SPI_POLARITY_LOW  0
#define SPI_POLARITY_HIGH 1

#define SPI_PHASE_1EDGE 0
#define SPI_PHASE_2EDGE 1

using SPI_HandleTypeDef = int;
