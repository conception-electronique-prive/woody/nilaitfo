﻿/**
 ******************************************************************************
 * @addtogroup adsModule
 * @{
 * @file    adsModule
 * @author  Samuel Martel
 * @brief   Header for the adsModule module.
 *
 * @date 9/2/2020 8:22:54 AM
 *
 ******************************************************************************
 */
#ifndef _adsModule
#    define _adsModule
#    if defined(NILAI_USE_ADS) && defined(NILAI_USE_SPI)
/*****************************************************************************/
/* Includes */
#        include "shared/interfaces/adsModuleConfig.h"

#        include "shared/defines/misc.hpp"
#        include "shared/defines/module.hpp"

#        include "shared/drivers/spiModule.hpp"

#        include <array>
#        include <string>
#        include <vector>

/*****************************************************************************/
/* Exported defines */

/*****************************************************************************/
/* Exported macro */

/*****************************************************************************/
/* Exported types */
enum class TrigEdge
{
    Alter   = 0,    // Voltage goes above or bellow trigger level.
    Rising  = 1,    // Voltage rises above trigger level.
    Falling = 2,    // Voltage falls bellow trigger level.
};

struct AdsPacket
{
    uint32_t timestamp = 0;
    uint32_t duration  = 0;

    struct Channel
    {
        float avg;
        float min;
        float max;
        float rms;
    };

    std::array<Channel, 4> channels;
};

class SystemModule;

/**
 ** @note   Currently not as universal as it could be.
 **         The interface assumes that:
 **             - SPI mode is always Asynchronous Slave
 **             - Device Words are always 24-bits wide
 **             - Hamming is always off
 **             - Frame Mode is always fixed (6 words long)
 **             - VREF is always 2.442V
 ** */
class AdsModule : public cep::Module
{
public:
    AdsModule(SpiModule* spi, std::string label);
    ~AdsModule() override = default;

    bool               DoPost() override;
    void               Run() override;
    const std::string& GetLabel() const override { return m_label; }

    [[maybe_unused]] [[nodiscard]] const ADS::Config& GetConfig() const { return m_config; }

    [[maybe_unused]] [[nodiscard]] const AdsPacket& GetLatestPacket() const
    {
        return m_latestFrame;
    }

    [[maybe_unused]] [[nodiscard]] float GetAverageChannel(size_t idx) const
    {
        return m_latestFrame.channels[idx].avg;
    }
    [[maybe_unused]] [[nodiscard]] float GetMinChannel(size_t idx) const
    {
        return m_latestFrame.channels[idx].min;
    }
    [[maybe_unused]] [[nodiscard]] float GetMaxChannel(size_t idx) const
    {
        return m_latestFrame.channels[idx].max;
    }
    [[maybe_unused]] [[nodiscard]] float GetRmsChannel(size_t idx) const
    {
        return m_latestFrame.channels[idx].rms;
    }
    [[maybe_unused]] [[nodiscard]] uint32_t GetLatestDuration() const
    {
        return m_latestFrame.duration;
    }

    // Use force = true if you want the config to be applied no matter what.
    void Configure(const ADS::Config& config = ADS::Config(), bool force = false);
    void Enable();
    void Disable();

    // Using timeout = 0 skips the waiting for DRDY
    void RefreshValues();
    inline float     CalculateTension(const uint8_t* data);

    [[nodiscard]] bool IsActive() const { return m_active; }

    void SetSamplesToTake(uint16_t samplesToTake, uint16_t samplesToIgnore = 0)
    {
        m_samplesToTake   = samplesToTake;
        m_samplesToIgnore = samplesToIgnore;
    }

    void SetCallback(const std::function<void(const AdsPacket&)>& cb) { m_callback = cb; }

private:
    bool        m_active = false;
    SpiModule*  m_spi;
    std::string m_label;
    ADS::Config m_config;
    bool        m_isConfigured = false;
    AdsPacket   m_latestFrame;
    uint32_t    m_lastStartTime = 0;

    uint8_t  m_trigChannel  = 0;
    float    m_trigLevel    = 0.0f;
    TrigEdge m_trigEdge     = TrigEdge::Alter;
    bool     m_hasTriggered = false;

    uint16_t                              m_samplesToTake   = 1;
    uint16_t                              m_samplesToIgnore = 0;
    uint16_t                              m_samplesTaken    = 0;
    std::function<void(const AdsPacket&)> m_callback;

private:
    static const uint8_t MaxInitAttempts = 10;

private:
    inline void         Reset() const;
    inline bool         SendCommand(uint16_t cmd, uint16_t expectedResponse);
    inline bool         SendConfig(uint8_t addr, uint8_t data);
    inline uint16_t     Send(uint16_t data);
    inline uint16_t     ReadCommandResponse();
    static inline float ConvertToVolt(int32_t val);
    inline uint32_t     ConvertToHex(float val);
    void                UpdateLatestFrame();
};

/*****************************************************************************/
/* Exported functions */

/* Have a wonderful day :) */
#    endif /* _adsModule */
#endif
/**
 * @}
 */
/****** END OF FILE ******/
